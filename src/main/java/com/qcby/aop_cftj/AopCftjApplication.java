package com.qcby.aop_cftj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AopCftjApplication {

	public static void main(String[] args) {
		SpringApplication.run(AopCftjApplication.class, args);
	}

}
