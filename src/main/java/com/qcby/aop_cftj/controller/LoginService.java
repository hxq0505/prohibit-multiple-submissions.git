package com.qcby.aop_cftj.controller;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.UUID;
@Service
public class LoginService {
    @Resource
    RedisTemplate<String, String> redisTemplate;
    public String login(String phone, String password, HttpServletResponse response){
//        redisTemplate.opsForValue().set(phone,password);
        //签名密钥
        String signature = "hello";
        JwtBuilder builder = Jwts.builder();
        //使用JWT生成器创建一个JWT
        String jwtToken = builder
                //Header
                .setHeaderParam("typ", "JWT")
                //类型
                .setHeaderParam("alg", "HS256")
                //使用的算法
                //Payload
                .claim("phone", phone)
                .setSubject("admin-test")
                .setExpiration(new Date(System.currentTimeMillis() + 1000*60*60*24))
                //失效日期：当前时间+24小时
                .setId(UUID.randomUUID().toString())
                //signature
                .signWith(SignatureAlgorithm.HS256, signature)
                //使用的算法+签名密钥
                //调用compact()方法将三部分拼接起来并用'.'分隔
                .compact();
        Cookie cookie=new Cookie("token",jwtToken.substring(0,5));
        response.addCookie(cookie);
        return "登录成功！！";
    }
}
